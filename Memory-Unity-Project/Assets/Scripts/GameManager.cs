﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class GameManager : MonoBehaviour
{
    // Variable that holds the currentl level #
    public static int CurrentLevelNumber;
    // This indicates that the enum can be treated as a bit field
    [System.Flags]
    public enum Symbols
    {
        Waves =             1 << 0,
        Dot =               1 << 1,
        Square =            1 << 2,
        LargeDiamond =      1 << 3,
        SmallDiamonds =     1 << 4,
        Command =           1 << 5,
        Bomb =              1 << 6,
        Sun =               1 << 7,
        Bones =             1 << 8,
        Drop =              1 << 9,
        Face =              1 << 10,
        Hand =              1 << 11,
        Flag =              1 << 12,
        Disk =              1 << 13,
        Candle =            1 << 14,
        Wheel =             1 << 15
    }

    // Creates a struct to hold the information used to build the level
    //Used for quickly assigning and referencing the values needed for each level from the inspector
    [System.Serializable]
    public struct LevelDescription
    {
        public int Rows, Columns;
        [EnumFlags]
        public Symbols UsedSymbols;
    }

    

    // Creates a field in the inspector for the Card prefab
    public GameObject TilePrefab;
    // Creates a field for the tile spacing value
    public float TileSpacing;

    // Creates a new list of Structs called Levels
    public LevelDescription[] Levels = new LevelDescription[0];
    //Delegate that returns void
    public Action HideTilesEvent;

    // Creates a member variable for the Tile class called  m_tileOne and m_tileTwo
    //to refer to the tiles chosen by the player.
    private Tile m_tileOne, m_tileTwo;
    // An int value that represents the number of cards left in the game
    private int m_cardsRemaining;
    public int noMatch;
    public int yesMatch;
    private int timesClicked;
    static private int totalClicks;
    static public int totalYesMatch;
    static public int totalNoMatch;
    static private int tileClicked;
    float levelTime;
    private float startTime;

    private void Start()
    {
        //Runs LoadLevel() based on the level defined by LevelComplete();
        LoadLevel( CurrentLevelNumber);
        float startTimeM = Time.time / 60;
        float startTimeS = Time.time % 60;
        startTime = Time.time;
    }

    private void OnApplicationQuit()
    {
        float endTimeM = Time.time/60;
        float endTimeS = Time.time % 60;
        AnalyticsEvent.Custom("Levels Completed Before Quitting", new Dictionary<string, object>
        {
            {"Levels Completed:", (CurrentLevelNumber)},
            {"End Time", (Mathf.RoundToInt(endTimeM) + ":" + Mathf.RoundToInt(endTimeS))},
            {"Total Clicks:", totalClicks},
            {"Total Matches:", totalYesMatch},
            {"Total Mismatches:", totalNoMatch}
        });
    }

    private void Clicked()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) == true)
        {
            timesClicked++;
            totalClicks++;
        }
        
    }



    private void Update()
    {
        Clicked();
    }

    //
    private void LoadLevel( int levelNumber )
    {
       
        // Increases the number of level chosen each time this method is called.
        LevelDescription level = Levels[levelNumber % Levels.Length];
        
        // Sets up a list of symbols based on the ones chosen from the inspector for the corresponding level defined by GetRequiredSymbols().
        List<Symbols> symbols = GetRequiredSymbols( level );

        //Method that instantiates the Card prefabs in the correct places based on their offsets
        for ( int rowIndex = 0; rowIndex < level.Rows; ++rowIndex )
        {
            float yPosition = rowIndex * ( 1 + TileSpacing );
            for ( int colIndex = 0; colIndex < level.Columns; ++colIndex )
            {
                float xPosition = colIndex * ( 1 + TileSpacing );
                GameObject tileObject = Instantiate( TilePrefab, new Vector3( xPosition, yPosition, 0 ), Quaternion.identity, this.transform );
                int symbolIndex = UnityEngine.Random.Range( 0, symbols.Count );
                // Gets the renderer component so that it can adjust the material of the card to display the proper symbol
                tileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol( symbols[symbolIndex] );
                // Communicates with the Tile component of the game object to pass it the GameManager object and what symbol it is supposed to be.
                tileObject.GetComponent<Tile>().Initialize( this, symbols[symbolIndex] );
                // Removes the chosen symbol from the list of available ones so that it is not chosen too many times.
                symbols.RemoveAt( symbolIndex );
            }
        }

        SetupCamera( level );
    }

    // Creates a list of symbols based on the ones selected in the inspector for the level
    private List<Symbols> GetRequiredSymbols( LevelDescription level )
    {
        // creates a new list called symbols
        List<Symbols> symbols = new List<Symbols>();
        // Defines how many cards there are in the current level
        int cardTotal = level.Rows * level.Columns;

        // 
        {
            Array allSymbols = Enum.GetValues( typeof( Symbols ) );
            //Defines how many cards are left in the game
            m_cardsRemaining = cardTotal;

            // If there is any remainder, throws up an error in the console
            if ( cardTotal % 2 > 0 )
            {
                new ArgumentException( "There must be an even number of cards" );
            }

            // Add the symbols chosen in the inspector to a list called symbols 
            foreach ( Symbols symbol in allSymbols )
            {
                // 0 = chosen, 1 = not chosen
                if ( ( level.UsedSymbols & symbol ) > 0 )
                {
                    symbols.Add( symbol );
                }
            }
        }

        // Throws up an error in the console if the symbol count is off
        {
            if ( symbols.Count == 0 )
            {
                new ArgumentException( "The level has no symbols set" );
            }
            if ( symbols.Count > cardTotal / 2 )
            {
                new ArgumentException( "There are too many symbols for the number of cards." );
            }
        }

      
        {
            // Defines half the number of total cards compared to the number of symbols chosen
            int repeatCount = ( cardTotal / 2 ) - symbols.Count;
            // If the repeatCount is more than the number of symbols valilable,
            // do the following so that there are enough symbols
            if ( repeatCount > 0 )
            {
                // Create two lists called symbolsCopy and duplicateSymbols
                List<Symbols> symbolsCopy = new List<Symbols>( symbols );
                List<Symbols> duplicateSymbols = new List<Symbols>();
                // For each symbol needed,
                for ( int repeatIndex = 0; repeatIndex < repeatCount; ++repeatIndex )
                {
                    // Choose a random symbol from the list of chosen symbols...
                    int randomIndex = UnityEngine.Random.Range( 0, symbolsCopy.Count );
                    // and add it to duplicateSymbols
                    duplicateSymbols.Add( symbolsCopy[randomIndex] );
                    // Remove it from the other list
                    symbolsCopy.RemoveAt( randomIndex );
                    // If there are no more symbols left to copy over...
                    if ( symbolsCopy.Count == 0 )
                    {
                        // Add more.
                        symbolsCopy.AddRange( symbols );
                    }
                }
                // Add what was just generated to the list of symbols
                symbols.AddRange( duplicateSymbols );
            }
        }
        // Double it so that maches can be made
        symbols.AddRange( symbols );

        // Return the list of total symbols
        return symbols;
    }

    // Determines what the offset of the material should be based on what the symbol is
    private Vector2 GetOffsetFromSymbol( Symbols symbol )
    {
        Array symbols = Enum.GetValues( typeof( Symbols ) );
        for ( int symbolIndex = 0; symbolIndex < symbols.Length; ++symbolIndex )
        {
            if ( ( Symbols )symbols.GetValue( symbolIndex ) == symbol )
            {
                return new Vector2( symbolIndex % 4, symbolIndex / 4 ) / 4f;
            }
        }
        Debug.Log( "Failed to find symbol" );
        return Vector2.zero;
    }

    // Sets the parameters needed for the camera to capture all of the tiles in the game based on the level
    private void SetupCamera( LevelDescription level )
    {
        Camera.main.orthographicSize = ( level.Rows + ( level.Rows + 1 ) * TileSpacing ) / 2;
        Camera.main.transform.position = new Vector3( ( level.Columns * ( 1 + TileSpacing ) ) / 2, ( level.Rows * ( 1 + TileSpacing ) ) / 2, -10 );
    }

   
    // When a tile is selected...
    public void TileSelected( Tile tile )
    {
        // if what was clicked has the Tile component on it...
        if ( m_tileOne == null )
        {
            tileClicked++;
            // assign the clicked tile's Tile component to m_tileOne...
            m_tileOne = tile;
            // and trigger Reveal() from the selected tile
            m_tileOne.Reveal();
        }
        // Same as above with the second tile. 
        else if ( m_tileTwo == null )
        {
            tileClicked++;
            m_tileTwo = tile;
            m_tileTwo.Reveal();
            // If the selected tiles have the same symbol...
            if ( m_tileOne.Symbol == m_tileTwo.Symbol )
            {
                // start the WaitForHide coroutine and pass it the following values.
                StartCoroutine( WaitForHide( true, 1f ) );
            }
            else
            {
                // start the WaitForHide coroutine and pass it the following values.
                StartCoroutine( WaitForHide( false, 1f ) );
            }
        }
    }

    // Determines if the game should end or progress to the next level
    private void LevelComplete()
    {
        float matchRate = yesMatch / (float)(yesMatch + noMatch)*100;
        Debug.Log(matchRate);
        AnalyticsEvent.Custom("Level Statistics", new Dictionary<string, object>
            {
                {"Level #:", (CurrentLevelNumber + 1)},
                {"Matches Made", yesMatch},
                {"Mismatches Made", noMatch},
                {"Success Rate:", matchRate},
            });

        AnalyticsEvent.Custom("Click Statistics", new Dictionary<string, object>
            {
                {"Level #:", (CurrentLevelNumber + 1)},
                {"Times Clicked:", timesClicked},
                {"Times Clicked on a Tile", tileClicked}
            });

        float endTime = Time.time;
        AnalyticsEvent.Custom("Time To Complete Level", new Dictionary<string, object> {
            {"Time", ((Mathf.RoundToInt((endTime - startTime)/60) )+ ":" + (Mathf.RoundToInt((endTime - startTime)%60)))}
        });
        ++CurrentLevelNumber;
        timesClicked = 0;
        yesMatch = 0;
        noMatch = 0;
        tileClicked = 0;
        endTime = 0;
        startTime = Time.time;
        // if you just completed the last level...
        if ( CurrentLevelNumber > Levels.Length - 1 )
        {
            // send GameOver in the debug console.
            Debug.Log( "GameOver" );
            float overallMatchRate = 100 * (totalYesMatch / (float)(totalYesMatch + totalNoMatch));
            AnalyticsEvent.Custom("Overall Level Statistics", new Dictionary<string, object>
            {
                {"Total Clicks:", totalClicks},
                {"Total Matches:", totalYesMatch},
                {"Total Mismatches:", totalNoMatch},
                {"Overall Success Rate:", (overallMatchRate + "%")}
            });
        }
        // If you didn't complete the last level...
        else
        {
            // load the next scene.

            SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex );
        }
    }

    
    private IEnumerator WaitForHide( bool match, float time )
    {
        // Create a timer value
        float timer = 0;
        // While the timer is less than the time of the value passed to the coroutine,...
        while ( timer < time )
        {
            // increase the timer.
            timer += Time.deltaTime;
            // If the timer value exceeds what was passed to this coroutine...
            if ( timer >= time )
            {
                // stop the coroutine.
                break;
            }
            // return null
            yield return null;
        }
        // if the two chosen tiles match...
        if ( match )
        {
            // destroy them and reduce the amount of cards left in the game
            yesMatch++;
            totalYesMatch++;
            Debug.Log(yesMatch);
            Destroy( m_tileOne.gameObject );
            Destroy( m_tileTwo.gameObject );
            m_cardsRemaining -= 2;
        }
        // If the cards did not match...
        else
        {
            // flip them back over
            noMatch++;
            totalNoMatch++;
            Debug.Log(noMatch);

            m_tileOne.Hide();
            m_tileTwo.Hide();
        }
        // Empty the tileOne and tileTwo variable so they can be used again.
        m_tileOne = null;
        m_tileTwo = null;

        // If there are no cards remaining in the level...
        if ( m_cardsRemaining == 0 )
        {
            // run LevelComplete() to either progress or end the game.
            LevelComplete();
        }
    }
}
