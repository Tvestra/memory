﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
    //
    private GameManager m_manager;
    public GameManager.Symbols Symbol { get; private set; }
    private Transform m_child;
    private Collider m_collider;

    
    private void Start()
    {
        // Sets the variable m_child to be the transform of the first child attached to this game object.
        m_child = this.transform.GetChild( 0 );
        // Sets the variable m_collider as a shortcut for obtaining the collider component
        m_collider = GetComponent<Collider>();
    }

    //When the mouse is pressed down, triggers the TileSelected method in the game manager
    //and sends it the game object this script is attached to.
    private void OnMouseDown()
    {
        m_manager.TileSelected( this );
    }

    //Recieves the game manager object and the symbol it is supposed to be from the GameManager Script
    public void Initialize( GameManager manager, GameManager.Symbols symbol )
    {
        m_manager = manager;
        Symbol = symbol;
    }

    // When TileSelected is triggered, it in turn triggers this method
    // to start the coroutine to flip the gameObject this script is attached to
    // It additionally surns off the collider component.
    public void Reveal()
    {
        StopAllCoroutines();
        // Hands Spin the values it needs to flip the card properly
        StartCoroutine( Spin( 180, 0.8f ) );
        m_collider.enabled = false;
    }

    // If the IEnumerator WaitForHide in the GameManager script 
    // confirms that the selected tiles do not match,
    // it triggers this method to stop itself and flip the cards back over.
    // Additionally it re-enables the collider component on the object
    public void Hide()
    {
        StopAllCoroutines();
        // Hands Spin the values it needs to flip the card properly
        StartCoroutine( Spin( 0, 0.8f ) );
        m_collider.enabled = true;
    }

    // This is the coroutine that flips the cards
    private IEnumerator Spin( float target, float time )
    {
        // sets the timer to 0
        float timer = 0;
        //Sets the reference for starting position
        float startingRotation = m_child.eulerAngles.y;
        // creates a variable that can be used to modify the rotation of the game object
        Vector3 euler = m_child.eulerAngles;
        // While the timer is less than the value given to this coroutine by either Reveal(), or Hide()...
        while ( timer < time )
        {
            // Increase the timer each second
            timer += Time.deltaTime;
            //rotate the value of the euler variable on its Y-axis from startingRotation to the target value
            //given by either Reveal() or Hide(), and do it within the time value given by the same method.
            euler.y = Mathf.LerpAngle( startingRotation, target, timer / time );
            // Make this game object's rotation equal to the euler variable so that it rotates
            m_child.eulerAngles = euler;
            yield return null;
        }
        euler.y = target;
        m_child.eulerAngles = euler;
    }
}
